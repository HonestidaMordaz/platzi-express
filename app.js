'use strict'

/*
 * Modules & dependencies
 */
const Http = require('http')
const Server = require('./lib/server')

const Config = require('./config.json')

let App = new Server()

/*
 * Magic happens!
 */
Http
	.createServer(App.express)
	.listen(Config.port)
