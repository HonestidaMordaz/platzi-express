const ServeFavicon = require('serve-favicon')
const Path = require('path')

module.exports = ServeFavicon(Path.join(__dirname, '../static/favicon.ico'))
