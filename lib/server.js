'use strict'

const Express = require('express')
const Middlewares = require('./middlewares/admin')

const Server = function (config) {
	config = config || {}
	
	this.express = Express()
	
	/*
	 * Middlewares
	 */
	for (let mid in Middlewares) {
		this.express.use(Middlewares[mid])
	}
	
	/*
	 * Routes
	 */
	this.express.get('/article/save/', function (req, res, next) {
		res.send(req.url)
	})
	
	this.express.get('/article/list/', function (req, res, next) {
		res.send(req.url)
	})
}

module.exports = Server
